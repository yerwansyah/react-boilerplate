# React Boiler Plate Template

## Synopsis
This project is a React Boiler Plate Template.

To use this project just run the following command:

```
npm install -g webpack
npm install
webpack
node server.js
```

## License
MIT License